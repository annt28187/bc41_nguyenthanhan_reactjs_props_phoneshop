import './App.css';
import PhoneShop from './components/PhoneShop';

function App() {
  return (
    <div>
      <PhoneShop />
    </div>
  );
}

export default App;
