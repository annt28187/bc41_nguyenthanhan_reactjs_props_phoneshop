import React, { Component } from 'react';
import { dataPhone } from './data_phone';
import DetailPhone from './DetailPhone';
import ListPhone from './ListPhone';

export default class PhoneShop extends Component {
  state = {
    dataPhone: dataPhone,
    detail: dataPhone[0],
  };
  render() {
    return (
      <div className="container">
        <h2 className="p-4">PhoneShop</h2>
        <ListPhone list={this.state.dataPhone} />
        <DetailPhone detail={this.state.detail} />
      </div>
    );
  }
}
