import React, { Component } from 'react';

export default class DetailPhone extends Component {
  render() {
    let { tenSP, manHinh, heDieuHanh, cameraTruoc, cameraSau, ram, rom, giaBan, hinhAnh } =
      this.props.detail;
    return (
      <div>
        <div className="row mt-5 alert-secondary p-5 text-left">
          <div className="col-3">
            <h5>{tenSP}</h5>
            <img src={hinhAnh} alt={tenSP} />
          </div>
          <div className="col-9">
            <h5>Thông số kỹ thuật</h5>
            <div className="row">
              <div className="col-3">
                <p>Màn hình</p>
                <p>Hệ điều hành</p>
                <p>Camera trước</p>
                <p>Camera sau</p>
                <p>RAM</p>
                <p>ROM</p>
                <p>Giá bán</p>
              </div>
              <div className="col-6">
                <p>{manHinh}</p>
                <p>{heDieuHanh}</p>
                <p>{cameraTruoc}</p>
                <p>{cameraSau}</p>
                <p>{ram}</p>
                <p>{rom}</p>
                <p>{giaBan}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
