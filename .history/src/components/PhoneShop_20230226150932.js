import React, { Component } from 'react';
import CartPhone from './CartPhone';
import { dataPhone } from './data_phone';
import DetailPhone from './DetailPhone';
import ListPhone from './ListPhone';

export default class PhoneShop extends Component {
  state = {
    dataPhone: dataPhone,
    detail: dataPhone[0],
    cart: [],
  };
  handleChangleDetailPhone = (phone) => {
    this.setState({
      detail: phone,
    });
  };
  render() {
    return (
      <div className="container">
        <h2 className="p-4">Phone Shop</h2>
        <CartPhone />
        <ListPhone
          list={this.state.dataPhone}
          handleChangleDetailPhone={this.handleChangleDetailPhone}
        />
        <DetailPhone detail={this.state.detail} />
      </div>
    );
  }
}
