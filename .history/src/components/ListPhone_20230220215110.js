import React, { Component } from 'react';
import ItemPhone from './ItemPhone';

export default class ListPhone extends Component {
  renderListPhone = () => {
    return this.props.list.map((item, index) => {
      return (
        <ItemPhone key={index} phone={item} handleClick={this.props.handleChangleDetailPhone} />
      );
    });
  };
  render() {
    return <div className="row">{this.renderListPhone()}</div>;
  }
}
