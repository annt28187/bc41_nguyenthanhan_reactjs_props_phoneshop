import React, { Component } from 'react';
import CartPhone from './CartPhone';
import { dataPhone } from './data_phone';
import DetailPhone from './DetailPhone';
import ListPhone from './ListPhone';

export default class PhoneShop extends Component {
  state = {
    dataPhone: dataPhone,
    detail: dataPhone[0],
    cart: [],
  };

  handleAddToCart = (phone) => {
    let cloneCart = { ...this.state.cart };
    let index = cloneCart.findIndex((item) => {
      return item.maSP === phone.maSP;
    });
    if (index === -1) {
      let cartItem = { ...phone, soLuong: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[index].soLuong++;
    }
  };

  handleChangleDetailPhone = (phone) => {
    this.setState({
      detail: phone,
    });
  };
  render() {
    return (
      <div className="container">
        <h2 className="p-4">Phone Shop</h2>
        <CartPhone cart={this.state.cart} />
        <ListPhone
          list={this.state.dataPhone}
          handleChangleDetailPhone={this.handleChangleDetailPhone}
        />
        <DetailPhone detail={this.state.detail} />
      </div>
    );
  }
}
