import React, { Component } from 'react';

export default class ItemPhone extends Component {
  render() {
    let { hinhAnh, tenSP, giaBan } = this.props.phone;
    return (
      <div className="col-3 p-4">
        <div className="card border-primary h-100">
          <img className="card-img-top" src={hinhAnh} alt={tenSP} />
          <div className="card-body">
            <h4 className="card-title">{tenSP}</h4>
            <p className="card-text">{giaBan}</p>
          </div>
          <button className="btn btn-success">Xem chi tiết</button>
        </div>
      </div>
    );
  }
}
