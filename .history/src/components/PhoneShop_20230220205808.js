import React, { Component } from 'react';
import DetailPhone from './DetailPhone';
import ListPhone from './ListPhone';

export default class PhoneShop extends Component {
  render() {
    return (
      <div className="container">
        <h2>PhoneShop</h2>
        <ListPhone />
        <DetailPhone />
      </div>
    );
  }
}
