import React, { Component } from 'react';
import CartPhone from './CartPhone';
import { dataPhone } from './data_phone';
import DetailPhone from './DetailPhone';
import ListPhone from './ListPhone';

export default class PhoneShop extends Component {
  state = {
    dataPhone: dataPhone,
    detail: dataPhone[0],
    cart: [],
  };

  handleAddToCart = (phone) => {
    let cloneCart = { ...this.state.cart };
    let index = cloneCart.findIndex((item) => {
      return item.maSP === phone.maSP;
    });
    if (index === -1) {
      let cartItem = { ...phone, soLuong: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[index].soLuong++;
    }
    this.setState({
      cart: cloneCart,
    });
  };

  handleDeleteToCart = (idPhone) => {
    let newCart = this.state.cart.filter((phone) => {
      return phone.maSP !== idPhone;
    });
    this.setState({
      cart: newCart,
    });
  };

  handleChangeQuantity = (idPhone, luaChon) => {
    let cloneCart = { ...this.state.cart };
    let index = cloneCart.findIndex((item) => {
      return item.maSP === idPhone;
    });
    if (index !== -1) {
      cloneCart[index].soLuong = cloneCart[index].soLuong + luaChon;
    }
    cloneCart[index].soLuong === 0 && cloneCart.splice(index, 1);
    this.setState({
      cart: cloneCart,
    });
  };

  handleChangleDetailPhone = (phone) => {
    this.setState({
      detail: phone,
    });
  };
  render() {
    return (
      <div className="container">
        <h2 className="p-4">Phone Shop</h2>
        <CartPhone
          cart={this.state.cart}
          handleChangeQuantity={this.handleChangeQuantity}
          handleDeleteToCart={this.handleDeleteToCart}
        />
        <ListPhone
          list={this.state.dataPhone}
          handleChangleDetailPhone={this.handleChangleDetailPhone}
          handleAddToCart={this.handleAddToCart}
        />
        <DetailPhone detail={this.state.detail} />
      </div>
    );
  }
}
