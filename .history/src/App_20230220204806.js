import './App.css';
import PhoneShop from './components/PhoneShop';

function App() {
  return (
    <div className="container">
      <PhoneShop />
    </div>
  );
}

export default App;
