import React, { Component } from 'react';

export default class ItemPhone extends Component {
  render() {
    let { hinhAnh, tenSP } = this.props.phone;
    return (
      <div>
        <div className="card h-100">
          <img className="card-img-top" src={hinhAnh} alt={tenSP} />
          <div className="card-body">
            <h4 className="card-title">{tenSP}</h4>
          </div>
          <button className="btn btn-success">Xem chi tiết</button>
        </div>
      </div>
    );
  }
}
